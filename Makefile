### Main

.PHONY:
rebuild: down build start

.PHONY:
init: start wait mysql-restore

### Shortcuts

.PHONY:
shell:
	docker exec -it url-app bash

.PHONY:
start:
	docker-compose up -d

.PHONY:
restart:
	docker-compose restart app

.PHONY:
build:
	docker-compose build

.PHONY:
down:
	docker-compose down

.PHONY:
logs:
	docker-compose logs -f

.PHONY:
wait:
	sleep 10

### Fixtures

.PHONY:
mysql-dump:
	docker exec url-mysql /usr/bin/mysqldump --no-tablespaces -u test --password=test url_db > ./fixtures/url_db.sql

.PHONY:
mysql-restore:
	cat ./fixtures/url_db.sql | docker exec -i url-mysql /usr/bin/mysql -u test --password=test url_db
