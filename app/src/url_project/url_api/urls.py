from django.urls import path, include

from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('clear/', views.clear, name='clear'),
    path('view/<str:alias>/', views.view, name='view'),
    path('urls/', views.UrlListView.as_view(), name='url_list'),
    path('create/', views.UrlCreateBasicView.as_view(), name='url_create_basic'),
    path('update/<str:alias>/', views.UrlBasicDetail.as_view(), name='url_update_basic'),
    path('premium/create', views.UrlCreatePremiumView.as_view(), name='url_create_premium'),
    path('premium/update/<str:alias>/', views.UrlPremiumDetail.as_view(), name='url_update_premium'),
]
