from rest_framework import serializers
from .models import Url


class UrlSerializer(serializers.ModelSerializer):
    """Serializer for list view."""
    class Meta:
        model = Url
        fields = ['id', 'alias', 'url', 'visited', 'created']


class UrlBasicSerializer(serializers.ModelSerializer):
    """Serializer for basic users."""
    class Meta:
        model = Url
        fields = ['id', 'url', 'created']


class UrlPremiumSerializer(serializers.ModelSerializer):
    """Serializer for premium users."""
    class Meta:
        model = Url
        fields = ['id', 'alias', 'url', 'created']
