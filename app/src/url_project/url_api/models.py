from django.db import models


class Url(models.Model):
    alias = models.SlugField(max_length=250, null=False, unique=True)
    url = models.URLField(max_length=250)
    visited = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.alias
