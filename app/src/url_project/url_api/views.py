import uuid
from datetime import datetime, timedelta

from django.shortcuts import render, redirect, get_object_or_404
from rest_framework import generics, permissions
from rest_framework.response import Response

from .serializers import UrlSerializer, UrlBasicSerializer, UrlPremiumSerializer
from .models import Url


def index(request):
    """Main HTML index page."""
    queryset = Url.objects.all()
    return render(request, 'url_api/index.html', {'urls': queryset})


def view(request, alias):
    """Increments url counter and redirects the user to object's url."""
    obj = get_object_or_404(Url, alias=alias)
    obj.visited += 1
    obj.save()
    return redirect(obj.url)


def clear(request):
    """Clear old urls."""
    time_threshold = datetime.now() - timedelta(days=30)
    queryset = Url.objects.filter(created__lt=time_threshold)
    for item in queryset:
        item.delete()
    return redirect('index')


class UrlListView(generics.ListAPIView):
    """URL-s list."""
    queryset = Url.objects.all()
    serializer_class = UrlSerializer


class UrlCreateBasicView(generics.CreateAPIView):
    """Create url with free/basic account."""
    serializer_class = UrlBasicSerializer

    def create(self, request):
        data = {
            'url': request.data.get('url'),
            'alias': str(uuid.uuid4())[:8],
            'visited': 0,
        }
        serializer = UrlSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        return redirect('url_update_basic', alias=data.get('alias'))


class UrlBasicDetail(generics.RetrieveUpdateDestroyAPIView):
    """Update urls with free/basic account."""
    queryset = Url.objects.all()
    serializer_class = UrlBasicSerializer
    lookup_field = 'alias'

    def retrieve(self, request, alias):
        queryset = Url.objects.get(alias=alias)
        serializer = UrlBasicSerializer(queryset, many=False)
        return Response(serializer.data)


class UrlCreatePremiumView(generics.CreateAPIView):
    """Create url with premium account."""
    serializer_class = UrlPremiumSerializer
    permission_classes = [permissions.IsAuthenticated]

    def create(self, request):
        data = {
            'url': request.data.get('url'),
            'alias': request.data.get('alias'),
            'visited': 0,
        }
        serializer = UrlSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
        return redirect('url_update_premium', alias=data.get('alias'))


class UrlPremiumDetail(generics.RetrieveUpdateDestroyAPIView):
    """Update url with premium account."""
    queryset = Url.objects.all()
    serializer_class = UrlPremiumSerializer
    lookup_field = 'alias'
    permission_classes = [permissions.IsAuthenticated]

    def retrieve(self, request, alias):
        queryset = Url.objects.get(alias=alias)
        serializer = UrlPremiumSerializer(queryset, many=False)
        return Response(serializer.data)
