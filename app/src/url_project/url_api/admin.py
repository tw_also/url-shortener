from django.contrib import admin
from .models import Url


@admin.register(Url)
class PostAdmin(admin.ModelAdmin):
    list_display = ('id', 'alias', 'url', 'visited', 'created')
    list_filter = ('visited', 'created')
    search_fields = ('alias', 'url')
    date_hierarchy = 'created'
    ordering = ('alias', 'url', 'visited', 'created')
